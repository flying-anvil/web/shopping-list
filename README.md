# Web-Skeleton

Skeleton for php-application. Uses docker as base.  
Consists of nginx, php, MySQL and phpmyadmin.

Ready for development

## Detach

```bash
rm -rf .git && git init
```
