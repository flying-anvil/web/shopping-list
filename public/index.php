<?php

echo sprintf(
    'Hello World from PHP %s.%s.%s',
    PHP_MAJOR_VERSION,
    PHP_RELEASE_VERSION,
    PHP_MINOR_VERSION,
);
